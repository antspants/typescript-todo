import { TodoItem } from './todoItem';

type itemCounts = {
  total: number,
  incomplete: number
}

export class TodoCollection {
  private nextId: number = 1;
  private itemMap = new Map<number, TodoItem>();

  constructor(public userName: string, private todoItems: TodoItem[] = []) {
    todoItems.forEach(item => this.itemMap.set(item.id, item))
  }

  addTodo(task: string) : number {
    while(this.getTodoById(this.nextId)) {
      this.nextId++;
    }
    // this.todoItems = this.todoItems.concat(new TodoItem(this.nextId, task));
    this.itemMap.set(this.nextId, new TodoItem(this.nextId, task))
    return this.nextId;
  }

  getTodoById(id: number) : TodoItem {
    // return this.todoItems.find(item => item.id === id)
    return this.itemMap.get(id)
  }

  getTodoItems(includeCompleted: boolean): TodoItem[] {
    return [...this.itemMap.values()]
      .filter(item => includeCompleted || !item.complete)
  }

  removeComplete() {
    this.itemMap.forEach(item => {
      if(item.complete)
        this.itemMap.delete(item.id)
    })
  }

  markComplete(id: number, complete: boolean) {
    const todoItem = this.getTodoById(id);
    if(todoItem) {
      todoItem.complete = complete;
    }
  }

  getItemCounts() {
    return {
      total: this.itemMap.size,
      incomplete: this.getTodoItems(false).length
    }
  }
}
