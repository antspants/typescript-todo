
import { TodoItem } from './todoItem';
import { TodoCollection } from './todoCollection';
import * as inquirer from 'inquirer';

let todos: TodoItem[] = [
  new TodoItem(1, 'Buy flowets'),
  new TodoItem(2, 'Go for a run'),
  new TodoItem(3, 'Clean house', true),
  new TodoItem(4, 'Call Jens')
]

let collection: TodoCollection = new TodoCollection('Ant',todos)

console.clear()
console.log(`${collection.userName}'s ToDo List`
  + `(${collection.getItemCounts().incomplete} jobs to do)`);

let newId: number = collection.addTodo('Have breakfast')
let todoItem: TodoItem = collection.getTodoById(newId);
// todoItem.printDetails()

collection.removeComplete()

collection.getTodoItems(true).forEach(item => item.printDetails())
console.log(`${collection.userName}'s ToDo List`
  + `(${collection.getItemCounts().incomplete} jobs to do)`);

collection.markComplete(todoItem.id, true)
collection.getTodoItems(true).forEach(item => item.printDetails())
console.log(`${collection.userName}'s ToDo List`
  + `(${collection.getItemCounts().incomplete} jobs to do)`);

let showCompleted = true;

const displayTodoList = (): void => {
  console.log(`${collection.userName}'s ToDo List`
    + `(${collection.getItemCounts().incomplete} jobs to do)`);
  collection
    .getTodoItems(showCompleted)
    .forEach(item => item.printDetails())
}


enum Commands {
  Add = 'Add Todo',
  Complete = 'Complete Task',
  Toggle = "Show / Hide Completed",
  Purge = 'Remove Completed Tasks',
  Quit = 'Quit'
}

const promptComplete = () => {
  console.clear()
  inquirer.prompt({
    type: 'checkbox',
    name: 'complete',
    message: 'Mark tasks complete',
    choices: collection
              .getTodoItems(showCompleted)
              .map(item => ({name: item.task, value: item.id, checked: item.complete}))
  })
    .then(answers => {
      let completedTasks = answers['complete'] as number[];
      collection
        .getTodoItems(true)
        .forEach(item => {
          collection.markComplete(item.id,
          completedTasks.find(id => id === item.id) !== undefined)
        })
      promptUser();
    })
}

const promptAdd = () => {
  inquirer.prompt({
    type: 'input',
    name: 'add',
    message: 'Enter task:'
  })
    .then(answers => {
      if(answers['add'] !== '')
        collection.addTodo(answers['add'])
      promptUser();

    })
}

const promptUser = (): void => {
  console.clear()
  displayTodoList()
  inquirer.prompt({
    type: 'list',
    name: 'command',
    message: 'choose option',
    choices: Object.values(Commands),
  })
    .then(answers => {
      switch(answers['command']) {
        case Commands.Add:
          promptAdd();
          break;
        case Commands.Complete:
          promptComplete();
          break;
        case Commands.Purge:
          collection.removeComplete();
          promptUser();
          break;
        case Commands.Toggle:
          showCompleted = !showCompleted;
          promptUser();
          break;
        default:
          promptUser();
      }
    })
}

promptUser()
