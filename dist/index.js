"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const todoItem_1 = require("./todoItem");
const todoCollection_1 = require("./todoCollection");
const inquirer = require("inquirer");
let todos = [
    new todoItem_1.TodoItem(1, 'Buy flowets'),
    new todoItem_1.TodoItem(2, 'Go for a run'),
    new todoItem_1.TodoItem(3, 'Clean house', true),
    new todoItem_1.TodoItem(4, 'Call Jens')
];
let collection = new todoCollection_1.TodoCollection('Ant', todos);
console.clear();
console.log(`${collection.userName}'s ToDo List`
    + `(${collection.getItemCounts().incomplete} jobs to do)`);
let newId = collection.addTodo('Have breakfast');
let todoItem = collection.getTodoById(newId);
// todoItem.printDetails()
collection.removeComplete();
collection.getTodoItems(true).forEach(item => item.printDetails());
console.log(`${collection.userName}'s ToDo List`
    + `(${collection.getItemCounts().incomplete} jobs to do)`);
collection.markComplete(todoItem.id, true);
collection.getTodoItems(true).forEach(item => item.printDetails());
console.log(`${collection.userName}'s ToDo List`
    + `(${collection.getItemCounts().incomplete} jobs to do)`);
let showCompleted = true;
const displayTodoList = () => {
    console.log(`${collection.userName}'s ToDo List`
        + `(${collection.getItemCounts().incomplete} jobs to do)`);
    collection
        .getTodoItems(showCompleted)
        .forEach(item => item.printDetails());
};
var Commands;
(function (Commands) {
    Commands["Add"] = "Add Todo";
    Commands["Complete"] = "Complete Task";
    Commands["Toggle"] = "Show / Hide Completed";
    Commands["Purge"] = "Remove Completed Tasks";
    Commands["Quit"] = "Quit";
})(Commands || (Commands = {}));
const promptComplete = () => {
    console.clear();
    inquirer.prompt({
        type: 'checkbox',
        name: 'complete',
        message: 'Mark tasks complete',
        choices: collection
            .getTodoItems(showCompleted)
            .map(item => ({ name: item.task, value: item.id, checked: item.complete }))
    })
        .then(answers => {
        let completedTasks = answers['complete'];
        collection
            .getTodoItems(true)
            .forEach(item => {
            collection.markComplete(item.id, completedTasks.find(id => id === item.id) !== undefined);
        });
        promptUser();
    });
};
const promptAdd = () => {
    inquirer.prompt({
        type: 'input',
        name: 'add',
        message: 'Enter task:'
    })
        .then(answers => {
        if (answers['add'] !== '')
            collection.addTodo(answers['add']);
        promptUser();
    });
};
const promptUser = () => {
    console.clear();
    displayTodoList();
    inquirer.prompt({
        type: 'list',
        name: 'command',
        message: 'choose option',
        choices: Object.values(Commands),
    })
        .then(answers => {
        switch (answers['command']) {
            case Commands.Add:
                promptAdd();
                break;
            case Commands.Complete:
                promptComplete();
                break;
            case Commands.Purge:
                collection.removeComplete();
                promptUser();
                break;
            case Commands.Toggle:
                showCompleted = !showCompleted;
                promptUser();
                break;
            default:
                promptUser();
        }
    });
};
promptUser();
